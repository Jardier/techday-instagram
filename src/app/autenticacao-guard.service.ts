import { CanActivate } from "@angular/router";
import { Injectable } from "@angular/core";
import { AutenticacaoService } from "./acesso/autenticacao.service";

@Injectable()
export class AutenticacaoGuardService implements CanActivate {

    constructor(private service: AutenticacaoService){}
    
    canActivate() : boolean {
        return this.service.autenticado();
    }

}