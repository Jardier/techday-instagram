
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
 
  title = 'techday-instagram';

  ngOnInit(): void {
    var config = {
      apiKey: "AIzaSyCnq0kIu-v3gOv42qR0JtrMGX4PfL9hw5I",
      authDomain: "techday-instagram-45425.firebaseapp.com",
      databaseURL: "https://techday-instagram-45425.firebaseio.com",
      projectId: "techday-instagram-45425",
      storageBucket: "techday-instagram-45425.appspot.com",
      messagingSenderId: "663738302221"
    };
    firebase.initializeApp(config);
  }
 
}
