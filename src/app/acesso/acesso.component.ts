import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acesso',
  templateUrl: './acesso.component.html',
  styleUrls: ['./acesso.component.css']
})
export class AcessoComponent implements OnInit {

  public cadastro : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  public exibirlPainel(evento : string) : void {
    this.cadastro = evento === 'cadastro' ? true : false;
  }
}
