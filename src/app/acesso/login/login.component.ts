import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AutenticacaoService } from '../autenticacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output()
  public exibirPainel : EventEmitter<string> = new EventEmitter<string>();

  public formulario: FormGroup;

  
  constructor(private service: AutenticacaoService) { }

  ngOnInit() {
    this.formulario = new FormGroup({
      email: new FormControl(''),
      senha: new FormControl('')
    })
  }

  exibirPainelCadastro() : void {
    this.exibirPainel.emit('cadastro');
  
  }

  public autenticar() : void {
    const email = this.formulario.value.email;
    const senha = this.formulario.value.senha;

    this.service.autenticar(email,senha);
    
  }
}
