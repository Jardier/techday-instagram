import { Injectable } from "@angular/core";
import { Usuario } from "./cadastro/usuario.model";

import * as firebase from 'firebase';
import { Router } from "@angular/router";

@Injectable()
export class AutenticacaoService {

    public tokenId : string;

    constructor(private router: Router) {}

    public cadastrarusuario(usuario : Usuario) : Promise<any> {
        
        return firebase.auth()
                .createUserWithEmailAndPassword(usuario.email, usuario.senha)
                .then((resp) => {
                
                    delete usuario.senha;

                    firebase.database()
                            .ref(`detalhe_usuario/${btoa(usuario.email)}`)
                            .set(usuario)
                            .then((resposta) => console.log("Detalhes", resposta));
                
                })
                .catch((erro : Error) => {
                    console.log(erro);
                })
    }

    public autenticar(email: string, senha:string): void {
        firebase.auth()
                .signInWithEmailAndPassword(email,senha)
                .then((user: any) => {

                    firebase.auth()
                            .currentUser.getIdToken()
                            .then((tokenId:string) => {
                                this.tokenId = tokenId;
                                
                                localStorage.setItem('tokenId',tokenId);
                                this.router.navigate(['/home']);

                            })
                            .catch()
                })
                .catch((erro: Error) => console.log(erro));
    }

    public autenticado() : boolean {
        
        if(this.tokenId === undefined && localStorage.getItem('tokenId') !== null) {
            this.tokenId = localStorage.getItem('tokenId');
        }

        if(this.tokenId === undefined) {
            this.router.navigate(['/']);
        }

        return this.tokenId !== undefined;

    }
}