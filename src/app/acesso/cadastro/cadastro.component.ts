import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Usuario } from './usuario.model';
import { AutenticacaoService } from '../autenticacao.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  @Output()
  public exibirPainel : EventEmitter<string> = new EventEmitter<string>()

  public formulario : FormGroup;

  private emailPattern =  /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;

  constructor(
    private service : AutenticacaoService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      email : this.formBuilder.control('',[Validators.required, Validators.pattern(this.emailPattern)]),
      email_confirmacao : this.formBuilder.control('',[Validators.required, Validators.pattern(this.emailPattern)]),
      nome_completo : this.formBuilder.control('',[Validators.required, Validators.minLength(7)]),
      nome : this.formBuilder.control('', Validators.required),
      senha : this.formBuilder.control('', Validators.required)
    },{validator : CadastroComponent.equalsTo});
  }

  public exibirPainelLogin() : void {
    this.exibirPainel.emit("login");
  }

  public cadastroUsuario() : void {
    let usuario = new Usuario(
      this.formulario.value.email,
      this.formulario.value.nome_completo,
      this.formulario.value.nome,
      this.formulario.value.senha
    )

    this.service.cadastrarusuario(usuario).then(() => this.exibirPainelLogin());
  }

  public get nomeCompleto() : any {
    return this.formulario.get('nome_completo');
  }

  public static equalsTo(group : AbstractControl) : {[key:string]: boolean} {
    let email =  group.get('email');
    let emailConfirmacao = group.get('email_confirmacao');
    
    if(!email || !emailConfirmacao) {
      return undefined;
    }
    if(email.value !== emailConfirmacao.value) {
      return {emailsNotMatch: true}
    }

    return undefined;
  }

}
