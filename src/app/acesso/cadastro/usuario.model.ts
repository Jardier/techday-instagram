export class Usuario {
    
    constructor(
        public email : string,
        public nomeCompleto : string,
        public nome : string,
        public senha : string
    ){}
}